# Loading Container

Show center loading with white overlay

<img src="https://gitlab.com/jorgesanure-pub-dep/loading-container/-/raw/master/assets/preview.gif" height='300px'>

[DEMO](https://dartpad.dartlang.org/4ec62a7aa5cffbd43e201020e688a1ba?)

```dart
LoadingContainer(
    isLoading: false
    // continue loading after isLoading turns false
    delay: Duration(seconds: 2),
    child: Text('Data Here...'),
)
```

You can see a complete sample in `lib/main.dart` file