import 'package:flutter/material.dart';

import 'loading_container.dart';

void main() => runApp(MaterialApp(
        home: Scaffold(
      body: Center(
        child: FutureBuilder(
          future: Future.delayed(Duration(seconds: 1)),
          builder: (context, snapshot) {
            // Begin of loading container
            return LoadingContainer(
              isLoading: snapshot.connectionState == ConnectionState.waiting,
              // continue loading after 'isLoading' turns false
              delay: Duration(seconds: 2),
              child: Text(
                'Data Here...',
                style: TextStyle(fontSize: 50),
              ),
            );
            // End of loading container
          },
        ),
      ),
    )));
