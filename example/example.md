```dart
LoadingContainer(
    isLoading: true
    // continue loading after isLoading turns false
    delay: Duration(seconds: 2),
    child: Text('Data Here...'),
)
```